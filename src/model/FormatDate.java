package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {

	public String format(Date dateNote){
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.format(dateNote);
	}
	
	public String getOnlyDay(Date dateNote){
		String[] day = this.format(dateNote).split("/");
		return day[0];
	}
	
	public String getOnlyMonth(Date dateNote){
		String[] month = this.format(dateNote).split("/");
		return month[1];
	}
	
	public String getOnlyYear(Date dateNote){
		String[] year = this.format(dateNote).split("/");
		return year[2];
	}
}
