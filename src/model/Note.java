package model;

import java.util.ArrayList;
import java.util.Date;

import com.itextpdf.text.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import controller.FilterNotes;
import controller.ListerNotes;
import controller.writerReader.ReaderXML;

/**
 *A classe note é o modelo para a criação dos arquivos xml e tambem para a criação de notações,
 *nela estão contidos os metodos:
 * readTitleNote();
 * readMetaNote();
 * readDateNote();
 * readBodyNote();
 **/

@XStreamAlias("note")
public class Note {
	
	private String title;
	private ArrayList<String> meta;
	private Date dateNote;
	private String body;
		
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ArrayList<String> getMeta() {
		return meta;
	}

	public void setMeta(ArrayList<String> meta) {
		this.meta = meta;
	}

	public Date getDateNote() {
		return dateNote;
	}

	public void setDateNote(Date dateNote) {
		this.dateNote = dateNote;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	
	//Aqui os metodos de que acessam as classe filterNotes;
	
	public Note readTitleNote(String title){
		Note returner = new FilterNotes().filterTitleNote(title);		
		return returner;
	}

	public ArrayList<Note> readMetaNote(ArrayList<String> meta){
		ArrayList<Note> returner = new FilterNotes().filterMetaNote(meta);
		return returner;
	}
	
	public ArrayList<Note> readDateNote(Date datenote){
		ArrayList<Note> returner = new FilterNotes().filterDateNote(datenote);
		return returner;
	}
	
	public ArrayList<Note> readBodyNote(String word){
		ArrayList<Note> returner = new FilterNotes().filterBodyNote(word);		
		return returner;
	}

}//


















