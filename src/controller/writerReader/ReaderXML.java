package controller.writerReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import model.Note;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;

/*
 * A classe ReaderXML tem como função fazer a leitura de um arquivo xml e retornar uma notação
 * 
 * */

public class ReaderXML {
	
	public Note reader(String nameFileNote){
		//Aqui é dito para a classe XStream que será buscado um arquivo xml nos padroes da classe Note;
		XStream xStream = new XStream(new Dom4JDriver());
		xStream.alias("note", Note.class);
		xStream.processAnnotations(Note.class);
		Note note = null;
		try {
			//Aqui faz-se a leitura do arquivo desejado 
			BufferedReader input = new BufferedReader(new FileReader("notas/"+nameFileNote));
			//Aqui ocorre a atribuição dos elementos do arquivo para um objeto Note;
			note = (Note) xStream.fromXML(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return note;
	}

}
