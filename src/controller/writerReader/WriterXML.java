package controller.writerReader;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import model.Note;

import com.thoughtworks.xstream.XStream;
/**
 * A classe WriterXML cria e grava um arquivo xml com as tags de acordo com a classe Note
 * */
public class WriterXML {

	
	public void writer(Note note){
		
		//Aqui é instanciado um XStream e atribuaido um alias a ele sobre a classe Note;
		XStream xStream = new XStream();
		xStream.alias("note", Note.class);
		
		//Aqui ele cria o arquivo com o nome do title, para uma melhor organização de nomenclaturas;
		File arquivo = new File("notas/"+note.getTitle()+".xml");
		FileOutputStream gravar;
		try {
			gravar = new FileOutputStream(arquivo);
			gravar.write(xStream.toXML(note).getBytes());
			gravar.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
