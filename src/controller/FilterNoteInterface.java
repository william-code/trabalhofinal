package controller;

import java.util.ArrayList;
import java.util.Date;

import model.Note;

public interface FilterNoteInterface {

	
	public Note filterTitleNote(String title);

	public ArrayList<Note> filterMetaNote(ArrayList<String> meta);
	
	public ArrayList<Note> filterDateNote(Date datenote);
	
	public ArrayList<Note> filterBodyNote(String word);
	
}
