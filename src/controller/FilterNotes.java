package controller;

import java.util.ArrayList;
import java.util.Date;

import model.Note;

/**
 * A classe FilterNote Faz a filtragem das notações ela recebe um texto no formato padronizado de 
 * notação para facil identificação dos elementos contidos na notação.
 * Os padrões de notação são os seguntes:
 * 
 * para titulo -> title:titulo da nota;
 * para palavras chave -> meta:palavra 1,chave1, palavra chave2;
 * para data -> date: dd-mm-yyyy;
 * 
 * aqui exemplo de um bloco de notação completo:
 * 
 * header[
 * 
 * 		title: Exemplo de Nota;
 * 		meta: exemplo, filter, notas;
 * 		date: 18/07/2015;
 * ]
 * body[
 * 
 * 		Este é o corpo da notação onde será descrito alguma
 * 		informação que se deseja ser guardada.
 * ] 
 * 
 */

public class FilterNotes implements FilterNoteInterface{

	public Note filterTitleNote(String title){
		//Aqui é feito uma requisição para a lista de notas
		ListerNotes list = new ListerNotes().searchListNotes();
		Note returner = null;
		for (Note note : list.getListNotes()) {
			//se o titulo da respectiva nota for igual ao procurado return, ignora-se maiusculas e minusculas;
			if(note.getTitle().equalsIgnoreCase(title))
			returner = note;
		}
		
		return returner;
	}

	public ArrayList<Note> filterMetaNote(ArrayList<String> metas){
		ListerNotes list = new ListerNotes().searchListNotes();
		ArrayList<Note> returner = new ArrayList<Note>();
		for (Note note : list.getListNotes()) {
			//se no corpo da notação existir o que se procura entao adiciona-se a lista de notas que possuem o que se busca
			for (String meta : metas) {
				if(note.getMeta().contains(meta) && !returner.contains(note)){
					returner.add(note);
				}
			}
		}
		
		return returner;
	}
	
	public ArrayList<Note> filterDateNote(Date dateNote){
		ListerNotes list = new ListerNotes().searchListNotes();
		ArrayList<Note> returner = new ArrayList<Note>();
		for (Note note : list.getListNotes()) {
			if(note.getDateNote().equals(dateNote)){
				returner.add(note);
			}
		}		
		return returner;
	}
	
	public ArrayList<Note> filterBodyNote(String word){
		ListerNotes list = new ListerNotes().searchListNotes();
		ArrayList<Note> returner = new ArrayList<Note>();
		for (Note note : list.getListNotes()) {
			//se no corpo da notação existir o que se procura entao adiciona-se a lista de notas que possuem o que se busca
			if(note.getBody().contains(word)){
				returner.add(note);
			}
		}
		
		return returner;
	}
}
