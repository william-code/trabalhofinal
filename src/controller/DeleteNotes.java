package controller;

import java.io.File;


public class DeleteNotes {
	private File baseFolder;
	public void deleterNotes(String title){
		try {
			// diretório que será listado.
			baseFolder = new File("/home/william/workspace/trabalhofinal/notas");

			// obtem a lista de arquivos
			File[] files = baseFolder.listFiles();
			for (File file : files) {
				if (file.getPath().endsWith(".xml") == true && file.getName().equals(title+".xml")) {
					file.delete();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
