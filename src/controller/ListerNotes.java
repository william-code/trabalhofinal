package controller;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import controller.writerReader.ReaderXML;
import model.Note;

/*
 * A classe ListerNotes traz uma lista de Notações;
 * */

public class ListerNotes {

	private ArrayList<Note> listNotes = new ArrayList<Note>();
	private FileWriter list = null;
	private File baseFolder = null;
	
	public ArrayList<Note> getListNotes(){
		return listNotes;
	}
	
	public ListerNotes searchListNotes() {
		try {
			// arquivo onde será gravada a lista de arquivos do diretório
			list = new FileWriter(new File("notesList.txt"));

			// diretório que será listado.
			baseFolder = new File("/home/william/workspace/trabalhofinal/notas");

			// obtem a lista de arquivos
			File[] files = baseFolder.listFiles();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				if (file.getPath().endsWith(".xml") == true) {
					String name = file.getName();
					list.write(name + "\n");
					list.flush();
					//System.out.println(file.getName());
					Note note = new ReaderXML().reader(file.getName());
					listNotes.add(note);
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return this;
	}

}
