package view;

import javax.swing.JFileChooser;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import java.io.File; 
import java.io.FileInputStream;
import java.io.IOException;


public class Buscador {
	
	public static void main (String[] args){
		JFileChooser buscador = new JFileChooser();
		buscador.setCurrentDirectory(new File(System.getProperty("user.home")));
		int result = buscador.showOpenDialog(buscador);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = buscador.getSelectedFile();
	    
			try {
				if (selectedFile.exists()) {
					PdfReader pdfReader = new PdfReader(new FileInputStream(selectedFile));
					String teste = PdfTextExtractor.getTextFromPage(pdfReader, 1);
					System.out.println(teste);
				}
			}  catch( IOException e) { 
				System.out.println("erro");
			}
		}
	}
}