package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import model.FormatDate;
import model.Note;
import controller.DeleteNotes;
import controller.FilterNotes;
import controller.ListerNotes;
import controller.writerReader.WriterXML;

public class JNote extends JFrame {

	private JPanel contentPane;
	private JButton btnNewNote;
	private JButton btnEditNote;
	private JButton btnFind;
	private JRadioButton findTitle;
	private JRadioButton findDate;
	private JRadioButton findMeta;
	private JTextField textTitle;
	private JTextField textData;
	private JTextField textMeta;
	private JTextField textFind;
	private JTextArea textArea;
	private JButton btnRemoveNote;
	private JTable tableNotes;
	private JTable tableFilter;
	private JScrollPane scrollPane;
	ListerNotes listerNotes = new ListerNotes().searchListNotes();
	
	
	public static void main(String[] args) {
		JNote jNote = new JNote();
		jNote.setVisible(true);
	}

	/**
	 * Create the frame.
	 */
	public JNote() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 50, 1160, 950);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new FlowLayout());
		setContentPane(contentPane);
		
		String[][] dados = new String[listerNotes.getListNotes().size()][3];
		
		for(int i=0; i < listerNotes.getListNotes().size(); i++){
			dados[i][0] = listerNotes.getListNotes().get(i).getTitle();
			dados[i][1] = new FormatDate().format( listerNotes.getListNotes().get(i).getDateNote());
			String metas = "";
			for (String meta : listerNotes.getListNotes().get(i).getMeta()) {
				metas = meta +"," +metas;
			}
			dados[i][2] = metas;
		}
		
		//ListerNotes listerNotes = new ListerNotes().searchListNotes();
		for (int i = 0; i < listerNotes.getListNotes().size(); i++) {
			System.out.println(listerNotes.getListNotes().get(i).getTitle());
		}
		
		 tableNotes = new JTable();
		 
		 tableNotes.setModel(new javax.swing.table.DefaultTableModel(
		            dados,
		            new String [] {
		                "Titulo", "Data", "Palavras chave"
		            }
		        ));
		 
         scrollPane = new JScrollPane(tableNotes);
         contentPane.add(scrollPane); 
         
         textArea = new JTextArea(30, 50);
         contentPane.add(textArea);
		
		textTitle = new JTextField("nome",20);
		contentPane.add(textTitle);
		
		textData = new JTextField("dd/MM/yyyy",20);
		contentPane.add(textData);
		
		textMeta = new JTextField("Palavras chave",20);
		contentPane.add(textMeta);
         
        btnNewNote = new JButton("Nova Nota");
		btnNewNote.setSize(30, 60);
		contentPane.add(btnNewNote);
		
		btnRemoveNote = new JButton("Excluir Nota");
		contentPane.add(btnRemoveNote);
		
		btnEditNote = new JButton("Editar");
		contentPane.add(btnEditNote);
		
		String[][] dadosFilter = new String[listerNotes.getListNotes().size()][3];
		
		for(int i=0; i < listerNotes.getListNotes().size(); i++){
			dadosFilter[i][0] = listerNotes.getListNotes().get(i).getTitle();
			dadosFilter[i][1] = new FormatDate().format( listerNotes.getListNotes().get(i).getDateNote());
			String metas = "";
			for (String meta : listerNotes.getListNotes().get(i).getMeta()) {
				metas = meta +"," +metas;
			}
			dados[i][2] = metas;
		}
		
		//ListerNotes listerNotes = new ListerNotes().searchListNotes();
		for (int i = 0; i < listerNotes.getListNotes().size(); i++) {
			System.out.println(listerNotes.getListNotes().get(i).getTitle());
		}
		 
		tableFilter = new JTable(50, 80);
		 
		 tableFilter.setModel(new javax.swing.table.DefaultTableModel(new Object[][] {},
		            new String [] {
		                "Titulo", "Data", "Palavras chave"
		            }
		        ));
		 
         scrollPane = new JScrollPane(tableFilter);
         contentPane.add(scrollPane);       
         
         textFind = new JTextField(20);
         contentPane.add(textFind);
         
         btnFind = new JButton("Pesquisar");
 		 contentPane.add(btnFind);
 		 
 		 findTitle = new JRadioButton("Pesquisar por Título");
 		 contentPane.add(findTitle);
 		 
	 	 findDate = new JRadioButton("Pesquisar por Data");
		 contentPane.add(findDate);
		 
		 findMeta = new JRadioButton("Pesquisar por Palavra chave");
 		 contentPane.add(findMeta);
 		 
 		  		 
		
		/**
		 * 
		 * Inicio dos eventos
		 * 
		 * @author william
		 * 
		 */
 		
 		class listenerFind implements ActionListener{
 			DefaultTableModel var = (DefaultTableModel) tableFilter.getModel();	
 			
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
							
				if (var.getRowCount() > 0){
				   int i = 0;
			       do{
			    	   var.removeRow(i);
			        	System.out.println("numero da row" + var.getRowCount());
			        	System.out.println("valor do i" + i);
			            i++;
			        }while(i < var.getRowCount());              
				}
				
				Note titleFind;
				ArrayList<Note> dateFind = new ArrayList<Note>();
				ArrayList<Note> metaFind = new ArrayList<Note>();
				String inputFind = textFind.getText().trim();
				FilterNotes filterNotes = new FilterNotes();
				if(findTitle.isSelected() && !findMeta.isSelected() && !findDate.isSelected()){
					titleFind = filterNotes.filterTitleNote(inputFind);
					ArrayList<Note> noteTitleFind = new ArrayList<Note>();
					noteTitleFind.add(titleFind);
					finder(noteTitleFind);
				}else if(findDate.isSelected()){				
					dateFind = filterNotes.filterDateNote(new Date(inputFind));
					finder(dateFind);
				}else if(findMeta.isSelected()){
					String[] metaslist = inputFind.split(",");
					ArrayList<String> metasFind = new ArrayList<String>();
					for (String meta : metaslist) {
						metasFind.add(meta);
						System.out.println(meta);
					}
					metaFind = filterNotes.filterMetaNote(metasFind);
					finder(metaFind);
				}
			}
			
			public void finder(ArrayList<Note> notesListFind){
				//ListerNotes notesFind = new ListerNotes().searchListNotes();
				for (Note note : notesListFind) {
					System.out.println(note.getTitle());
					String metas = "";
					for (String meta : note.getMeta()) {
						metas = meta +"," +metas;
					}
			        var.addRow(new String[]{note.getTitle(), new FormatDate().format(note.getDateNote()), metas});
				}
			}
 			
 		}
 		btnFind.addActionListener(new listenerFind());
		
//		Listener para criar novo Note.
		class ListenerNewNote implements ActionListener{

			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				String title = textTitle.getText().trim();
				String date = textData.getText().trim();
				String meta = textMeta.getText().trim();
				String body = textArea.getText();
				String[] ArrMetas = meta.split(",");
				ArrayList<String> metas = new ArrayList<String>();
				for(String meta2 : ArrMetas){
					metas.add(meta2);
				}
				
				Note newNote = new Note();
				newNote.setTitle(title);
				newNote.setDateNote(new Date(date));
				newNote.setMeta(metas);
				newNote.setBody(body);
				
				WriterXML writer = new WriterXML();
				writer.writer(newNote);
		        
		        DefaultTableModel var = (DefaultTableModel) tableNotes.getModel();
		        var.addRow(new String[]{title, date, meta});
			}
			
		}
		btnNewNote.addActionListener(new ListenerNewNote());
		
		class ListenerRemoveNote implements ActionListener{

			public void actionPerformed(ActionEvent e) {
		        int linhaSelecionada = -1;
				linhaSelecionada = tableNotes.getSelectedRow();
				if (linhaSelecionada >= 0) {
					String title = tableNotes.getValueAt(tableNotes.getSelectedRow(), 0).toString().trim();
					((DefaultTableModel) tableNotes.getModel()).removeRow(tableNotes.getSelectedRow());
					DeleteNotes delete = new DeleteNotes();
					delete.deleterNotes(title);
					
				} else {
					JOptionPane.showMessageDialog(null, "É necesário selecionar uma linha.");
				}
			}
			
		}
		btnRemoveNote.addActionListener(new ListenerRemoveNote());
		
		class ListenerEditNote implements ActionListener{
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				int linhaSelecionada = -1;
				linhaSelecionada = tableNotes.getSelectedRow();
				if (linhaSelecionada >= 0) {
					String title = tableNotes.getValueAt(tableNotes.getSelectedRow(), 0).toString().trim();
					String date = tableNotes.getValueAt(tableNotes.getSelectedRow(), 1).toString().trim();
					String meta = tableNotes.getValueAt(tableNotes.getSelectedRow(), 2).toString();
					String[] ArrMetas = meta.split(",");
					ArrayList<String> metas = new ArrayList<String>();
					for(String meta2 : ArrMetas){
						metas.add(meta2);
					}
					String body = textArea.getText().trim();
					Note editnote = new Note();
					editnote.setTitle(title);
					editnote.setDateNote(new Date(date));
					editnote.setMeta(metas);
					editnote.setBody(body);
					
					WriterXML writer = new WriterXML();
					writer.writer(editnote);
					
				} else {
					JOptionPane.showMessageDialog(null, "É necesário selecionar uma linha.");
				}
			}
			
		}
		btnEditNote.addActionListener(new ListenerEditNote());
		
		class ListenerTableNotes implements ListSelectionListener{

			public void valueChanged(ListSelectionEvent e) {
				if (tableNotes.getSelectedRow() > -1) {
		            FilterNotes filterNotes = new FilterNotes();
		            textArea.setText(filterNotes.filterTitleNote(tableNotes.getValueAt(tableNotes.getSelectedRow(), 0).toString()).getBody());
		            System.out.println(tableNotes.getValueAt(tableNotes.getSelectedRow(), 2).toString());
		        }
								
			}			
		}
		tableNotes.getSelectionModel().addListSelectionListener(new ListenerTableNotes());
		
		class ListenerTableFilter implements ListSelectionListener{

			public void valueChanged(ListSelectionEvent e) {
				if (tableFilter.getSelectedRow() > -1) {
		            FilterNotes filterNotes = new FilterNotes();
		            textArea.setText(filterNotes.filterTitleNote(tableFilter.getValueAt(tableFilter.getSelectedRow(), 0).toString()).getBody());
		            System.out.println(tableFilter.getValueAt(tableFilter.getSelectedRow(), 2).toString());
		        }
								
			}			
		}
		tableFilter.getSelectionModel().addListSelectionListener(new ListenerTableFilter());
	}//end constructor
}//























